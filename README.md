# Polls

> Create ~~amazing~~ polls

Backend repo: <https://gitlab.com/the-zrzci/polls-backend>

## Installation

0. Install pnpm: `npm i -g pnpm`
1. Install dependencies: `pnpm i`
2. Start the dev server: `pnpm dev`

## Scripts

* Format everything: `pnpm format`
* Lint everything: `pnpm lint`
* Run e2e tests: `pnpm test:e2e:dev` (faster than prod)

## Important files

* `src/` - The app
* `tailwind.config.js` - Tailwind config - Theming etc.
