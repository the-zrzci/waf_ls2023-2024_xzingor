import { ref } from "vue";
import { defineStore } from "pinia";
import { type Poll, type PollCreate } from "@/models/APITypes";
import { API, APIResultError } from "@/API";

export const usePollStore = defineStore("poll", () => {
  const loading = ref(true);
  const poll = ref<Poll>();

  async function createPoll(body: PollCreate) {
    loading.value = true;
    try {
      await API.poll.createPoll(body);
    } catch (err) {
      if (err instanceof APIResultError) return err.code;
      throw err;
    } finally {
      loading.value = false;
    }
  }

  async function updatePoll(body: PollCreate, slug: string) {
    loading.value = true;
    try {
      await API.poll.updatePoll(body, slug);
    } catch (err) {
      if (err instanceof APIResultError) return err.code;
      throw err;
    } finally {
      loading.value = false;
    }
  }

  async function getPoll(slug: string) {
    loading.value = true;
    try {
      poll.value = await API.poll.getPoll(slug);
    } finally {
      loading.value = false;
    }
  }

  return { loading, createPoll, poll, getPoll, updatePoll };
});
