import { ref } from "vue";
import { defineStore } from "pinia";
import { APIError, type UserWithPolls } from "@/models/APITypes";
import { API, APIResultError, catchAPI } from "@/API";

export const useUserStore = defineStore("user", () => {
  const loading = ref(true);
  const user = ref<Readonly<UserWithPolls>>();

  /** Fetches currently signed user. */
  async function fetchUser() {
    loading.value = true;
    try {
      user.value = await API.user
        .getUser()
        // if not logged return undefined
        .catch(catchAPI(APIError.NOT_LOGGED_IN, () => undefined));
    } finally {
      loading.value = false;
    }
    return user.value;
  }

  async function login(username: string, password: string) {
    loading.value = true;
    try {
      const user = await API.user.login({ username, password });
      if (user) await fetchUser();
    } catch (err) {
      if (err instanceof APIResultError) return err.code;
      throw err;
    } finally {
      loading.value = false;
    }
  }

  async function register(username: string, password: string) {
    loading.value = true;
    try {
      const user = await API.user.register({ username, password });
      if (user) await fetchUser();
    } catch (err) {
      if (err instanceof APIResultError) return err.code;
      throw err;
    } finally {
      loading.value = false;
    }
  }

  async function logout() {
    loading.value = true;
    try {
      await API.user.logout();
      user.value = undefined;
    } finally {
      loading.value = false;
    }
  }

  // Fetch user on start
  const userPromise = fetchUser().catch(console.error);

  return { loading, user, login, register, logout, fetchUser, userPromise };
});
