import { BackgroundLine } from "@/lib/backgroundLine";
import { defineStore } from "pinia";

export const useBackgroundLines = defineStore("backgroundLines", () => {
  let lastConfig = "";
  const lines: ReturnType<typeof BackgroundLine>[] = [];

  function start(lineHeights: number[]) {
    const config = JSON.stringify(lineHeights);
    if (config == lastConfig) return;
    lastConfig = config;

    lines.length = 0;

    for (const line of lineHeights) {
      lines.push(BackgroundLine({ center: line }));
    }
  }

  return { start, lines };
});
