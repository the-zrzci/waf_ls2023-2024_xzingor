import { ref } from "vue";
import { defineStore } from "pinia";
import { APIError, type PollResults } from "@/models/APITypes";
import { API, APIResultError, API_ROUTES, getApiUrl } from "@/API";

export const useResultsStore = defineStore("results", () => {
  const loading = ref(false);
  const connected = ref(false);
  const poll = ref<PollResults>();
  let events: EventSource | null = null;

  /** Function called when a vote is added */
  const onVoteAdded = (ev: MessageEvent) => {
    if (!poll.value) return;

    const choices = JSON.parse(ev.data);
    for (const choiceId of choices) {
      // Find the updated choice
      const choice = poll.value.choices.find((c) => c.id == choiceId);
      if (!choice) {
        return console.warn(`Received update for choice ${choiceId} but it was not found!`);
      }
      // Update it
      choice.votes++;
    }
  };

  /** Function called when the poll is viewed */
  const onViewed = (_ev: MessageEvent) => {
    if (!poll.value) return;

    poll.value.views++;
  };

  /** Fetch the results and listen for updates */
  async function start(slug: string) {
    loading.value = true;

    // Reset just to be sure
    reset();

    try {
      // Fetch results
      poll.value = await API.poll.getResults(slug);

      // Start listening for updates
      events = new EventSource(getApiUrl(API_ROUTES.poll.sse(slug)));
      events.addEventListener("voteAdded", onVoteAdded);
      events.addEventListener("viewed", onViewed);
      events.addEventListener("open", () => (connected.value = true));
      events.addEventListener("error", () => (connected.value = false));
    } catch (err) {
      if (err instanceof APIResultError && err.code === APIError.NOT_FOUND) {
        poll.value = undefined;
      } else throw err;
    } finally {
      loading.value = false;
    }
  }

  /** Reset the store and stop listening for changes */
  function reset() {
    poll.value = undefined;
    connected.value = false;
    if (events) {
      events.close();
      events = null;
    }
  }

  return { results: poll, loading, connected, start, reset };
});
