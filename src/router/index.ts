import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import { useUserStore } from "@/stores/userStore";

declare module "vue-router" {
  interface RouteMeta {
    background?: boolean;
    fixedHeader?: boolean;
    authorized?: boolean;
    transition?: string;
    savedPosition?: { top: number; left: number } | null;
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  scrollBehavior(to, _from, savedPosition) {
    to.meta.savedPosition = savedPosition;
    return savedPosition ?? { top: 0 };
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
      meta: { background: true, fixedHeader: true },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/AuthView.vue"),
      meta: { background: true, fixedHeader: true },
    },
    {
      path: "/register",
      name: "register",
      component: () => import("../views/AuthView.vue"),
      meta: { background: true, fixedHeader: true },
    },
    {
      path: "/poll/create",
      name: "pollCreation",
      component: () => import("../views/polls/CreationView.vue"),
      meta: { authorized: true },
    },
    {
      path: "/poll/:slug/edit",
      name: "pollEdit",
      component: () => import("../views/polls/CreationView.vue"),
      meta: { authorized: true },
    },
    {
      path: "/poll/:slug",
      name: "vote",
      component: () => import("../views/polls/VoteView.vue"),
    },
    {
      path: "/poll/:slug/results",
      name: "results",
      component: () => import("../views/polls/ResultView.vue"),
    },
    {
      path: "/profile",
      name: "profile",
      component: () => import("../views/ProfileView.vue"),
      meta: { authorized: true },
    },
    {
      path: "/:catchAll(.*)",
      name: "notFound",
      component: () => import("../views/NotFoundView.vue"),
    },
  ],
});

router.beforeEach(async (to) => {
  const userStore = useUserStore();

  if (["login", "register"].includes(to.name as string)) {
    await userStore.userPromise;

    // Sign out if requested
    if (to.query.logOut) {
      await userStore.logout();
    }
    // Redirect from auth if already logged
    else if (userStore.user) {
      const redirectTo = to.query.redirectTo as string;
      // Redirect back
      if (redirectTo && !(await router.push(redirectTo))) return;
      // Or try to redirect
      if (to.redirectedFrom && !(await router.push(to.redirectedFrom))) return;
      // Else go to profile
      return await router.push({ name: "profile" });
    }
  } else if (to.meta.authorized) {
    await userStore.userPromise;

    // Check login
    if (!userStore.user) {
      return {
        name: "login",
        // save the location we were at to come back later
        query: { redirectTo: to.fullPath },
      };
    }
  }
});

export default router;
