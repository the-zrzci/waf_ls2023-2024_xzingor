import type { APIError } from "./models/APITypes";

export enum UIError {
  PASSWORD_MISMATCH = "PASSWORD_MISMATCH",
}

const API_ERROR_MESSAGE = {
  ALREADY_VOTED: "You have already voted for this poll!",
  TOO_MANY_CHOICES: "Too many choices!",
  NO_CHOICES: "Cannot have poll with no choices!",
  INVALID_SLUG: "Given slug is invalid!",
  NOT_FOUND: "Not found",
  NOT_LOGGED_IN: "You must log in first!",
  POLL_EXISTS: "Poll with given slug already exists!",
  USER_EXISTS: "User with given name already exists!",
  WRONG_PASSWORD: "Wrong username or password!",
} satisfies Record<APIError, string>;

const UI_ERROR_MESSAGE = {
  PASSWORD_MISMATCH: "Passwords do not match!",
} satisfies Record<UIError, string>;

export const ERROR_MESSAGE = {
  ...UI_ERROR_MESSAGE,
  ...API_ERROR_MESSAGE,
};

export function getError(error: string) {
  return ERROR_MESSAGE[error as keyof typeof ERROR_MESSAGE] ?? error;
}
