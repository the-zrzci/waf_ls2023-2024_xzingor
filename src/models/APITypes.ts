export enum APIError {
  WRONG_PASSWORD = "WRONG_PASSWORD", // Wrong username or password
  USER_EXISTS = "USER_EXISTS", // Username already in db
  NOT_LOGGED_IN = "NOT_LOGGED_IN", // User is not logged in
  POLL_EXISTS = "POLL_EXISTS", // Poll with given slug already exists
  NOT_FOUND = "NOT_FOUND", // Thing was not found
  ALREADY_VOTED = "ALREADY_VOTED",
  TOO_MANY_CHOICES = "TOO_MANY_CHOICES", // You're voting for too many choices
  NO_CHOICES = "NO_CHOICES", // Poll lacks choices
  INVALID_SLUG = "INVALID_SLUG", // Slug is not in valid format
}

export const SLUG_RE = /^\w+(-\w+)*$/;

// User stuff
export interface UserWithPassword {
  id: number;
  name: string;
  password: string;
}
export type User = Omit<UserWithPassword, "password">;

export type UserWithPolls = User & {
  polls: Poll[];
};

// Poll stuff
export interface Poll {
  id: number;
  authorId: number;
  slug: string;
  title: string;
  description?: string;
  maxChoices: number;
  views: number;
  duration: number;

  choices: Choice[];
}

export type PollCreate = Omit<Poll, "id" | "authorId" | "views" | "choices"> & {
  choices: ChoiceCreate[];
};

export type PollResults = Omit<Poll, "choices"> & {
  choices: (Choice & { votes: number })[];
};

export interface Choice {
  id: number;
  pollId: number;
  title: string;
  description?: string;
}

export type ChoiceCreate = Omit<Choice, "id" | "pollId"> & { id?: number };

export interface PollSSEEvents {
  /** Ids of the choices */
  voteAdded: number[];
  viewed: void;
}
