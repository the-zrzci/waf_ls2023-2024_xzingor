export interface BackgroundLineConfig {
  resolution: number;
  pointCount: number;
  maxChange: number;
  initialImpulse: number;
  maxVelocity: number;
  gravityStrength: number;
  gravityExp: number;
  drag: number;
  center: number;
}

export function BackgroundLine({
  pointCount = 4,
  maxChange = 0.001,
  initialImpulse = 0.1,
  maxVelocity = 0.005,
  gravityStrength = 0.001,
  gravityExp = 2,
  drag = 0.95,
  center = 0.5,
}: Partial<BackgroundLineConfig>) {
  const points: number[] = Array(pointCount).fill(center);
  const velocities: number[] = Array(pointCount).fill(0);

  let first = true;

  const update = () => {
    const change = first ? initialImpulse : maxChange;

    for (let i = 0; i < pointCount; i++) {
      const distance = center - points[i];
      const gravity =
        Math.pow(Math.abs(distance), gravityExp) * Math.sign(distance) * gravityStrength;
      velocities[i] += (Math.random() - 0.5) * change + (first ? 0 : gravity);
      points[i] = Math.max(0, Math.min(1, points[i] + velocities[i] * (first ? 1 : 1 - drag)));
      if (Math.abs(velocities[i]) > maxVelocity) {
        velocities[i] = Math.sign(velocities[i]) * maxVelocity;
      }
    }

    first = false;

    // Generate SVG path command
    return points;
  };

  return update;
}
