import * as APITypes from "./models/APITypes";

/** API routes */
export const API_ROUTES = {
  poll: {
    list: () => "/api/polls",
    single: (slug: string) => `/api/polls/${slug}`,
    vote: (slug: string) => `/api/polls/${slug}/vote`,
    results: (slug: string) => `/api/polls/${slug}/results`,
    sse: (slug: string) => `/api/polls/${slug}/sse`,
  },
  user: {
    get: () => "/api/user",
    login: () => "/api/user/login",
    register: () => "/api/user/register",
    logout: () => "/api/user/logout",
  },
} satisfies Record<string, Record<string, APIRoute<never[]>>>;

/**
 * API endpoint functions
 * @link https://gitlab.com/the-zrzci/polls-backend/-/tree/main
 */
export const API = {
  /** Poll endpoints */
  poll: {
    /** Create a new poll. Requires session cookie! */
    createPoll: createAPIRoute<{ id: number }, APITypes.PollCreate>("POST")(API_ROUTES.poll.list),
    /** Get the poll. */
    getPoll: createAPIRoute<APITypes.Poll>()(API_ROUTES.poll.single),
    /** Delete the poll. */
    deletePoll: createAPIRoute<{ ok: true }>("DELETE")(API_ROUTES.poll.single),
    /** Update the poll. */
    updatePoll: createAPIRoute<{ id: number }, APITypes.PollCreate>("PUT")(API_ROUTES.poll.single),
    /** Vote on a poll with given id. Session cookie optional, returns poll cookie. */
    vote: createAPIRoute<{ ok: true }, { choices: number[] }>("POST")(API_ROUTES.poll.vote),
    /** Get results */
    getResults: createAPIRoute<APITypes.PollResults>()(API_ROUTES.poll.results),
  },

  /** User endpoints */
  user: {
    /** Get info about the currently logged in user and their polls or 403 if not logged in. Requires session cookie! */
    getUser: createAPIRoute<APITypes.UserWithPolls>()(API_ROUTES.user.get),
    /** Logs in a user. */
    login: createAPIRoute<APITypes.User, LoginBody>("POST")(API_ROUTES.user.login),
    /** Registers and logs in a user. */
    register: createAPIRoute<APITypes.User, LoginBody>("POST")(API_ROUTES.user.register),
    /** Logs out the current user user. Requires session cookie! */
    logout: createAPIRoute<{ ok: true }>()(API_ROUTES.user.logout),
    /** Delete the current user */
    delete: createAPIRoute<{ ok: true }>("DELETE")(API_ROUTES.user.get),
  },
};

type LoginBody = { username: string; password: string };

export function getApiUrl(path: string) {
  return new URL(path, import.meta.env.VITE_API_URL);
}

///
/// DON'T LOOK BELLOW THIS LINE
////////////////////////////////////////////

type Scalar = number | string;

type APIRoute<Params extends Scalar[]> = (...params: Params) => string;
/** Promise that either resolves with T or error object */
type APIRouteFn<Result, Body = void, Params extends Scalar[] = []> = Body extends void
  ? (...params: Params) => Promise<Result>
  : (body: Body, ...params: Params) => Promise<Result>;

type Method = "GET" | "POST" | "PUT" | "DELETE";

/** Utility function that returns a function that returns a function that fetches given route */
function createAPIRoute<Result, Body = void>(method: Method = "GET") {
  return <Params extends Scalar[] = []>(path: APIRoute<Params>) =>
    (async (...params: [Body, ...Params]) => {
      // Decide what is body and what params according to path's argument length
      const pathParams = (path.length ? params.splice(-path.length) : []) as Params;
      const body = params.shift() as Body | undefined;

      // Create the URL
      const url = getApiUrl(path(...pathParams));

      // Send a request
      return fetch(url, {
        method,
        body: body ? JSON.stringify(body) : undefined,
        credentials: "include",
        mode: "cors",
      }).then(async (response) => {
        // Check for unexpected response
        if (response.headers.get("Content-Type") !== "application/json") {
          throw new APIUnexpectedError(response.status, await response.text());
        }

        // Parse result
        const data = await response.json();
        // if it's an error, throw it
        if (typeof data == "object" && "error" in data) {
          throw new APIResultError(response.status, data.error);
        }
        return data;
      });
    }) as APIRouteFn<Result, Body, Params>;
}

/** utility function to catch API errors */
export function catchAPI<R>(handler: (error: APIResultError) => R): (error: unknown) => R;
export function catchAPI<R>(
  type: APITypes.APIError,
  handler: (error: APIResultError) => R
): (error: unknown) => R;
export function catchAPI<R>(
  handlerOrType: ((error: APIResultError) => R) | APITypes.APIError,
  handler?: (error: APIResultError) => R
) {
  let _type: APITypes.APIError | null = null;
  let _handler: (error: APIResultError) => R;

  if (typeof handlerOrType === "function") {
    _handler = handlerOrType;
  } else {
    _type = handlerOrType;
    _handler = handler!;
  }

  return (error: unknown) => {
    if (error instanceof APIResultError && (!_type || error.code === _type)) {
      // Error matches
      return _handler(error);
    } else {
      // Unknown error
      throw error;
    }
  };
}

// Expose API object in browser in dev mode
if (import.meta.env.DEV) (window as unknown as { API: typeof API }).API = API;

/** When the API returns an error */
export class APIResultError extends Error {
  constructor(
    readonly status: number,
    readonly code: APITypes.APIError
  ) {
    super(code);
    this.name = "APIError " + status;
  }
}

/** When the API returns something weird */
export class APIUnexpectedError extends Error {
  constructor(
    readonly status: number,
    message?: string
  ) {
    super(message);
  }
}
