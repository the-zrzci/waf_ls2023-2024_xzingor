describe("Poll tests", () => {
  const random = Math.random().toString().replace("0.", "");
  const pollName = "Test poll " + random;
  const pollSlug = "test-slug-" + random;

  beforeEach(() => {
    // before each visit poll creation page
    cy.login().visit("/poll/create");
  });

  after(() => {
    cy.deleteUser();
  });

  it("should display the poll creation form", () => {
    cy.url().should("include", "/poll/create");
    cy.get('[data-cy="poll-name-input"]').should("be.visible");
    cy.get('[data-cy="poll-slug-input"]').should("be.visible");
    cy.get('[data-cy="poll-option-input"]').should("be.visible");
    cy.get('[data-cy="submit-button"]').should("be.visible");
  });

  it("should create a new poll with multiple options", () => {
    cy.get('[data-cy="poll-name-input"]').type(pollName);
    cy.get('[data-cy="poll-slug-input"]').clear().type(pollSlug);

    // Entering multiple options
    cy.get('[data-cy="poll-option-input"]').first().type("Option 1");
    cy.get('[data-cy="poll-option-input"]').eq(1).type("Option 2");
    cy.get('[data-cy="poll-option-input"]').eq(2).type("Option 3");

    cy.get('[data-cy="submit-button"]').click();

    // Verify poll creation
    cy.url().should("include", "/profile");
  });

  it("should show error for missing options", () => {
    cy.get('[data-cy="poll-name-input"]').type("New Poll Title");
    cy.get('[data-cy="submit-button"]').click();

    // Verify error message
    cy.get('[data-cy="error-answer"]').should("contain", "At least one answer must be provided");
  });

  it("should vote for a poll", () => {
    cy.visit("/poll/" + pollSlug);

    cy.contains("Option 1").click();
    cy.get('[data-cy="submit-button"]').click();

    cy.url().should("include", "/results");

    cy.contains("Option 1").parent().contains("1 votes");
  });

  it("should delete the created poll", () => {
    cy.visit("/profile");

    // Find and delete the created poll
    cy.contains(pollName)
      .parents('[data-cy="poll-item"]')
      .find('[data-cy="delete-button"]')
      .click();

    // Confirm delete in dialog
    cy.get('[data-cy="delete-dialog"]').should("be.visible");
    cy.get('[data-cy="confirm-delete-button"]').click();

    // Verify poll deletion
    cy.get('[data-cy="no-polls-message"]').should("exist");
  });
});
