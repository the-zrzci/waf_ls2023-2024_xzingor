describe("Authentication Tests", () => {
  beforeEach(() => {
    // Navigating to the Login Form Page
    cy.visit("/login");
  });

  it("renders login form", () => {
    // Verification that the login form contains an email field, password and button
    cy.get('[data-cy="title"]').should("contain", "Log In");
    cy.get('[data-cy="input-username-login"]').should("exist");
    cy.get('[data-cy="input-password-login"]').should("exist");
    cy.get('[data-cy="submit-button-login"]').should("exist");
  });

  it("displays an error message on invalid login", () => {
    //  Entering invalid login details
    cy.get('[data-cy="input-username-login"]').type("invalid-username");
    cy.get('[data-cy="input-password-login"]').type("wrong-password");
    cy.get('[data-cy="submit-button-login"]').click();

    // Verification that you receive an error message
    cy.get('[data-cy="alert-error"]').should("be.visible");
  });

  it("redirects to profile on successful login", () => {
    //  Entering valid login details
    cy.get('[data-cy="input-username-login"]').type("aaa");
    cy.get('[data-cy="input-password-login"]').type("aaa");
    cy.get('[data-cy="submit-button-login"]').click();

    // Verification that the user is redirected to the profile page
    cy.url().should("include", "/profile");
  });

  it("renders registration form", () => {
    // Clicking on the link to register
    cy.get('[data-cy="link"]').click();

    // Verification that the register form contains correct fields
    cy.get('[data-cy="title"]').should("contain", "Sign Up");
    cy.get('[data-cy="input-username-register"]').should("exist");
    cy.get('[data-cy="input-password-register"]').should("exist");
    cy.get('[data-cy="input-confirm-password-register"]').should("exist");
    cy.get('[data-cy="submit-button-register"]').should("exist");
  });

  it("displays an error message on password mismatch during registration", () => {
    // Clicking on the link to register
    cy.get('[data-cy="link"]').click();

    // Enter credentials with mismatched passwords
    cy.get('[data-cy="input-username-register"]').type("new-username");
    cy.get('[data-cy="input-password-register"]').type("password123");
    cy.get('[data-cy="input-confirm-password-register"]').type("password456");
    cy.get('[data-cy="submit-button-register"]').click();

    // Verification that you receive an error message
    cy.get('[data-cy="alert-error"]').should("be.visible");
  });

  it("redirects to profile on successful registration", () => {
    // Clicking on the link to register
    cy.get('[data-cy="link"]').click();

    // Entering valid register details
    cy.get('[data-cy="input-username-register"]').type("new-username");
    cy.get('[data-cy="input-password-register"]').type("password123");
    cy.get('[data-cy="input-confirm-password-register"]').type("password123");
    cy.get('[data-cy="submit-button-register"]').click();

    // Verification that the user is redirected to the profile page
    cy.url().should("include", "/profile");

    // Delete
    cy.deleteUser();
  });
});
