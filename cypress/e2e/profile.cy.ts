describe("Profile Page Tests", () => {
  beforeEach(() => {
    // before each login
    cy.login().visit("/profile");
  });

  after(() => {
    cy.deleteUser();
  });

  it("should display the user profile information", () => {
    cy.get('[data-cy="profile-header"]').should("contain", "Logged in as");
  });

  it("should display user polls", () => {
    cy.get('[data-cy="polls-title"]').should("contain", "Your polls");
    if (!Cypress.$('[data-cy="poll-item"]').length) {
      cy.get('[data-cy="no-polls-message"]').should("be.visible");
    } else {
      cy.get('[data-cy="polls-list"]').should("be.visible");
    }
  });

  it("should open delete dialog when delete button is clicked", () => {
    if (Cypress.$('[data-cy="poll-item"]').length) {
      cy.get('[data-cy="delete-button"]').first().click();
      cy.get('[data-cy="delete-dialog"]').should("be.visible");
      cy.get('[data-cy="delete-dialog-title"]').should("contain", "Delete poll?");
    }
  });

  it("should sign out when sign out button is clicked", () => {
    cy.get('[data-cy="sign-out-button"]').click();
    cy.url().should("include", "/login");
  });
});
