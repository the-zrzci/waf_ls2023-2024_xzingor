/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add("login", () => {
  cy.session(
    "random-user",
    () => {
      const username = "test-user-" + Math.random();

      cy.visit("/register");
      cy.get('[data-cy="input-username-register"]').type(username);
      cy.get('[data-cy="input-password-register"]').type(username);
      cy.get('[data-cy="input-confirm-password-register"]').type(username);
      cy.get('[data-cy="submit-button-register"]').click();
      cy.url().should("include", "/profile");
    },
    {
      validate() {
        cy.visit("/profile");
        cy.get('[data-cy="profile-header"]').should("exist");
      },
    }
  );
});

Cypress.Commands.add("deleteUser", () => {
  cy.window().then((window) => {
    // Delete account
    //@ts-expect-error
    window.API?.user.delete();
  });
});

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

export {};
