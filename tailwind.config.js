import defaultTheme from "tailwindcss/defaultTheme";

/** @type {import('tailwindcss').Config} */
export default {
  content: ["index.html", "src/**/*.{vue,.ts}"],
  theme: {
    extend: {
      colors: {
        accent: {
          light: "#FF3983",
          DEFAULT: "#9B50D6",
          dark: "#5D45AC",
          darkest: "#2A1938",
        },
        blue: {
          DEFAULT: "#2800C6"
        }
      },
      fontFamily: {
        sans: ["Lato", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
};
